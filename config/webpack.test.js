const PATH = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
const CONFIG = {
    src: './app',
    dev: './dev',
    dist: './dist'
};

module.exports = {
    entry: PATH.resolve(__dirname, CONFIG.app + '/components/main.js'),
    output: {
        filename: '[name].bundle.js',
        path: PATH.resolve(__dirname, config.dev),
        sourceMapFilename: '[name].map'
    },
    plugins: [
        new BundleAnalyzerPlugin({openAnalyzer: false}),
        new CommonsChunkPlugin({
            name: 'vendor',
            chunks: ['main'],
            minChunks: module => CONFIG.vendorsReg.test(module.resource)
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: CONFIG.vendorsReg,
                loader: 'babel-loader',
                query: {
                    optional: ['runtime'],
                    stage: 0,
                    cacheDirectory: '/tmp'
                }
            }
        ]
    }

};