const PATH = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
const CONFIG = {
    app: './app',
    dev: './dev',
    dist: './dist',
    vendorsReg: /node_modules|bower_components/
};

module.exports = {
    entry: PATH.resolve(__dirname, CONFIG.app + '/components/main.js'),
    output: {
        filename: '[name].bundle.js',
        path: PATH.resolve(__dirname, CONFIG.dev),
        sourceMapFilename: '[name].map'
    },
    plugins: [
        // new BundleAnalyzerPlugin({
        //   openAnalyzer: false,
        //   analyzerMode: 'static'
        // }),
        new CommonsChunkPlugin({
            name: 'vendor',
            chunks: ['main'],
            minChunks: module => CONFIG.vendorsReg.test(module.resource)
        })
    ],
    devServer: {
        contentBase: PATH.join(__dirname, CONFIG.dev),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: CONFIG.vendorsReg,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'es2015']
                    }
                }
            }
        ]
    }
};



// switch (process.env.NODE_ENV) {
//   case 'prod':
//   case 'production':
//     module.exports = require('./config/webpack.prod')({env: 'production'});
//     break;
//   case 'test':
//   case 'testing':
//     module.exports = require('./config/webpack.test')({env: 'test'});
//     break;
//   case 'dev':
//   case 'development':
//   default:
//     module.exports = require('./config/webpack.dev')({env: 'development'});
// }