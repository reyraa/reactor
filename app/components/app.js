import React from 'react';
import ReactDOM from 'react-dom';
import Widget from './widget';


class App extends React.Component {
    constructor () {
        super();
        this.state = {
            a: "this is the state text",
            b: "this is the state number"
        };
        this.update = this.update.bind(this);
    }
    update (e) {
        console.log(this.a)
        this.setState({
            a: this.a.input.value,
            b: this.b.input.value
        });
    };
    render () {
        return (
            <div>
                <Widget
                    ref={component => this.a = component}
                    update={this.update}
                    />
                <h2>{this.state.a}</h2>
                <Widget
                    ref={component => this.b = component}
                    update={this.update}
                     />
                <h2>{this.state.b}</h2>
            </div>
        );
    }
}

export default App;