import React from 'react';

class Listmaker extends React.Component {
    constructor () {
        super();
        this.state = {
            item: [],
            filter: ''
        };
    }
    componentWillMount () {
        fetch('http://swapi.co/api/people?format=json')
        .then(response => response.json())
        .then( ({results: items}) => {
            console.log('fetchig');
            console.log(items);
            return this.setState({items})
        });
    }
    filter (e) {
        this.setState({
            filter: e.target.value
        });
    }
    render () {
        let items = this.state.items ? this.state.items : [];
        if (this.state.filter) {
            items = items.filter(item => item.name.toLowerCase().includes(this.state.filter.toLowerCase()));
        }
        return (
            <div>
                <input onChange={this.filter.bind(this)} />
                <ul>
                    {items.map(item => <Person key={item.name} person={item} />)}
                </ul>
            </div>);
    }
}

const Person = (propes) => <li>{propes.person.name}</li>;

export default Listmaker;