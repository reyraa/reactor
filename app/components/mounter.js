import React from 'react';
import ReactDOM from 'react-dom';

class Incremetor extends React.Component {
    constructor () {
        super();
        this.state = {
            value: 0
        };
    }
    update () {
        this.setState({
            value: this.state.value + 1
        });
    }
    componentWillMount () {
        console.log('componentWillMount');
    }
    render () {
        console.log('render');
        return (
            <button onClick={this.update.bind(this)}>{this.state.value}</button>
        );
    }
    componentDidMount () {
        console.log('componentDidMount');
    }
    componentWillUnmount () {
        console.log('componentWillUnmount');
    }
}


class Mounter extends React.Component {
    constructor () {
        super();
    }
    mount () {
        ReactDOM.render(
            <Incremetor />,
            document.getElementById('a')
        );
    }
    unMount () {
        ReactDOM.unmountComponentAtNode(document.getElementById('a'));
    }
    render () {
        return (
            <div>
                <button onClick={this.mount.bind(this)}>Mount</button>
                <button onClick={this.unMount.bind(this)}>Unmount</button>
                <div id="a" />
            </div>
        );
    }
}



export default Mounter;