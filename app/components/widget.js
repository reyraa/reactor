import React from 'react';

class Widget extends React.Component {
    constructor () {
        super();
    }
    render () {
        return (
            <textarea ref={node => this.input = node} onInput={this.props.update}/>
        );
    }
}

export default Widget;