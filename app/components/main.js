import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import Mouter from './mounter';
import Listmaker from './list';

ReactDOM.render(<App />, document.getElementById('app'));
ReactDOM.render(<Mouter />, document.getElementById('mounter'));
ReactDOM.render(<Listmaker />, document.getElementById('listmaker'));
